from constants import *
from Databases.DBAbstract import *
from .settings import *

class neo4jDB(DBConnection):
  
  def getBorderCords(self):
    with driver.session() as session:
      minLatCoord, minLonCoord, maxLatCoord, maxLonCoord = session.read_transaction(self.query_getBorderCords)
      return minLatCoord, minLonCoord, maxLatCoord, maxLonCoord
  
  def query_getBorderCords(self,tx):
    for record in tx.run('MATCH (n) WHERE n.name="bbox" RETURN n.minlat,n.maxlat,n.minlon,n.maxlon'):
      minXCord=float(record["n.minlat"])
      maxXCord=float(record["n.maxlat"])
      minYCord=float(record["n.minlon"])
      maxYCord=float(record["n.maxlon"])
      
    return minXCord,minYCord,maxXCord,maxYCord
  
  def getAllObstacles(self):
    with driver.session() as session:
      return session.read_transaction(self.query_getAllObstacles)
  
  def query_getAllObstacles(self,tx):
    obstacles=[];
    currentObstacle=0
    query="MATCH (n) WHERE EXISTS(n.building) WITH n as buildings MATCH (buildings)<-[:TAGS]-(buildTagInfo)-[:FIRST_NODE]->()-[:NEXT *0..]->()-[:NODE]->(buildCorner) RETURN DISTINCT buildTagInfo.way_osm_id, buildCorner.lon, buildCorner.lat"
    for record in tx.run(query):
      if obstacles==[]:
        obstacles.append([record["buildTagInfo.way_osm_id"]])
      elif obstacles[currentObstacle][0]==record["buildTagInfo.way_osm_id"]:
        obstacles[currentObstacle].append([record["buildCorner.lat"],record["buildCorner.lon"]])
      else:
        currentObstacle+=1
        obstacles.append([record["buildTagInfo.way_osm_id"]])
        obstacles[currentObstacle].append([record["buildCorner.lat"],record["buildCorner.lon"]])
    return obstacles    
        
      
  def checkAreaForObstacle(self):
    with driver.session() as session:
      return session.read_transaction(self.query_checkAreaForObstacle)
    
  def query_checkAreaForObstacle(self,tx, lat_UpLeft,lon_UpLeft,lat_UpRight,lon_UpRight,lat_BotLeft, lon_BotLeft,lat_BotRight,lon_BotRight):
    #for record in tx.run("CALL spatial.bbox('buildings', {lon:"+str(lon_UpLeft)+", lat:"+str(lat_UpLeft)+"},{lon:"+str(lon_BotRight)+",lat:"+str(lat_BotRight)+"});"):
    query='WITH "POLYGON(('+str(lon_UpLeft)+' '+str(lat_UpLeft)+','+str(lon_UpRight)+' '+str(lat_UpRight)+','+str(lon_BotRight)+' '+str(lat_BotRight)+','+str(lon_BotLeft)+' '+str(lat_BotLeft)+','+str(lon_UpLeft)+' '+str(lat_UpLeft)+'))" as area CALL spatial.intersects("buildings",area) YIELD node RETURN node'
    for record in tx.run(query):
      return OBSTACLE #obstacle
    return FREE #No obstacle   
  
  def toConsole(self, msg):
    pass
  