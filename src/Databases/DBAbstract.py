import abc
class DBConnection(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def getBorderCords(self): 
      pass 
    
    @abc.abstractmethod
    def getAllObstacles(self): 
      pass
    
    @abc.abstractmethod
    def checkAreaForObstacle(self): 
      pass
    
    @abc.abstractmethod #Print to console always with modulename in front (i.e MAP: [MESSAGE])
    def toConsole(self,msg):
      pass
    