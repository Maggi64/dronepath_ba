import abc
class AbstractMap(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def loadCompleteMap(self): 
      pass
    @abc.abstractmethod
    def __init__(self,Database):
      pass
    
    @abc.abstractmethod #Print to console always with modulename in front (i.e MAP: [MESSAGE])
    def toConsole(self,msg):
      pass

class AbstractMapGrid(metaclass=abc.ABCMeta):
  
    @abc.abstractmethod
    def getfieldStatus(self): 
      pass
    
    @abc.abstractmethod
    def setFieldStatus(self): 
      pass