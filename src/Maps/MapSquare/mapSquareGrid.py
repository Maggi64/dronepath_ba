from osgeo import ogr
from pathlib import Path
import threading
import os
import json

from timeit import default_timer as timer

#AbstractClasses
from Maps.MapAbstract import *
from constants import *
from . settings import *
from helperFunc import calcDistance,convertXY_toCords,ranges
from Pathfinding.JumpPointSearch.jps_main import Pathfinder

#Load Database and turn it into a grid
class MapSquareGrid(AbstractMap,AbstractMapGrid):
  def __init__(self,Database):
    #Init not complete
    self.mapReady=False
    self.database= Database


    self.minLatCoord, self.minLonCoord, self.maxLatCoord, self.maxLonCoord = self.database.getBorderCords()
    self.toConsole(self.minLatCoord)
    self.toConsole(self.minLonCoord)
    self.toConsole(self.maxLatCoord)
    self.toConsole(self.maxLonCoord)
    self.obstacles= self.database.getAllObstacles()

    #Build Obstacle Polygon from
    if ON_THE_FLYDB==0:
      self.toConsole("Build Polygon from Obstacles")
      self._init_polygonOverlap()
      self.toConsole("Finished Polygon")

    #Calculate Grid
    distanceX=calcDistance(self.maxLatCoord,self.minLonCoord,self.minLatCoord, self.minLonCoord)
    distanceY=calcDistance(self.minLatCoord,self.minLonCoord,self.minLatCoord, self.maxLonCoord)
    self.toConsole("DistanceX_Map: "+str(distanceX))
    self.toConsole("DistanceY_Map: "+str(distanceY))
    xyRatio=distanceX/distanceY
    self.height=int(round(xyRatio*WIDTH, 0))
    self.squares_vert=int(SQUARES_HORI*xyRatio)

    self.tileSizeX=WIDTH/SQUARES_HORI
    self.tileSizeY=self.height/self.squares_vert


    loadMapThread=threading.Thread(target=self.loadCompleteMap, args=())
    loadMapThread.start()

  def _loadFromDB(self):
    self.toConsole("Load map from DB | Generating Grid")
    self.field = [[None for x in range(self.squares_vert)] for y in range(SQUARES_HORI)]
    self._loadMapChunk([0,SQUARES_HORI],[0,self.squares_vert])

    self.toConsole("Saving Map to json")
    map_file = open(Path('./Maps/MapSquare/'+MAP_FILENAME),"w")
    map_file.write(json.dumps(self.field))
    map_file.close()
    self.mapReady=True

  def loadCompleteMap(self):
      mapLoadTimerStart = timer()
      if USE_CACHE == 1:
        self.toConsole("Using Cache")
        if os.path.isfile(Path('./Maps/MapSquare/'+MAP_FILENAME)):
          self.toConsole("Load Map from cached file")
          map_file = open(Path('./Maps/MapSquare/'+MAP_FILENAME), 'r')
          fileStr=map_file.read()
          self.field=json.loads(fileStr)
          if not len(self.field)==SQUARES_HORI:
            self.toConsole("Invalid Cache->Rebuilding")
            self.rowStatus=0;
            self._loadFromDB()
          else:
            self.mapReady=True
        else:
          self.rowStatus=0;
          self._loadFromDB()
        self.toConsole("finished loading")
        mapLoadTimerEnd = timer()
        self.toConsole("MapGenTime: "+str(mapLoadTimerEnd-mapLoadTimerStart))
        self.mapReady=True

  def _loadMapChunk(self, ChunkRangeX, ChunkRangeY):
      CHUNK_RATIO=4
      chunkSizeY=ChunkRangeY[1]-ChunkRangeY[0]
      if chunkSizeY<=CHUNK_RATIO:
        for y in range(ChunkRangeY[0],ChunkRangeY[1]):
          self.getfieldStatus(ChunkRangeX[0], y)
      else:
        subChunkRangesY=ranges(chunkSizeY,CHUNK_RATIO)
        #speedUp building starting with bigger blocks
        for x in range(ChunkRangeX[0],ChunkRangeX[1]):
          if self.rowStatus<x:
            print("Row:"+ str(x),end='\r')
            self.rowStatus=x
          for chunk in subChunkRangesY:
            lat_UpLeft,lon_UpLeft = convertXY_toCords(x*self.tileSizeX,(chunk[0]+ChunkRangeY[0])*self.tileSizeY,self.maxLatCoord,self.minLatCoord,self.maxLonCoord,self.minLonCoord,WIDTH,self.height)
            lat_UpRight,lon_UpRight = convertXY_toCords(self.tileSizeX*(x+1),(chunk[0]+ChunkRangeY[0])*self.tileSizeY,self.maxLatCoord,self.minLatCoord,self.maxLonCoord,self.minLonCoord,WIDTH,self.height)
            lat_BotLeft, lon_BotLeft = convertXY_toCords(x*self.tileSizeX,(chunk[1]+ChunkRangeY[0])*self.tileSizeY,self.maxLatCoord,self.minLatCoord,self.maxLonCoord,self.minLonCoord,WIDTH,self.height)
            lat_BotRight,lon_BotRight = convertXY_toCords((x+1)*self.tileSizeX,(chunk[1]+ChunkRangeY[0])*self.tileSizeY,self.maxLatCoord,self.minLatCoord,self.maxLonCoord,self.minLonCoord,WIDTH,self.height)

            polycoord=[(lat_UpLeft,lon_UpLeft),(lat_UpRight,lon_UpRight),(lat_BotRight,lon_BotRight),(lat_BotLeft, lon_BotLeft)]
            if self.polygonOverlap(polycoord)==FREE:
              for z in range(chunk[0]+ChunkRangeY[0],chunk[1]+ChunkRangeY[0]):
                self.field[x][z]=FREE
            else:
              self._loadMapChunk([x,x+1], [chunk[0]+ChunkRangeY[0],chunk[1]+ChunkRangeY[0]])

  #----------------------------------------------------------------------------#
  #        Load Map from DB and generate Grid (currently not optimised)        #
  #----------------------------------------------------------------------------#
  def _init_polygonOverlap(self):
    #buildPolygons for intersectionchecking
    self._buildPolyCollection=[]

    for x in range(len(self.obstacles)):
      ring = ogr.Geometry(ogr.wkbLinearRing)

      for y in range(1,len(self.obstacles[x])):
        ring.AddPoint(self.obstacles[x][y][1], self.obstacles[x][y][0])
      #add first point again to close ring
      ring.AddPoint(self.obstacles[x][1][1], self.obstacles[x][1][0])
      # Create polygon #1
      poly = ogr.Geometry(ogr.wkbPolygon)
      poly.AddGeometry(ring)
      self._buildPolyCollection.append(poly)


  def polygonOverlap(self, polycoord):
    #create ring for polygon
    ring = ogr.Geometry(ogr.wkbLinearRing)
    for x in range(len(polycoord)):
      ring.AddPoint(polycoord[x][1],polycoord[x][0])
                            #lat      lon

    #Add first point at the end to close ring
    ring.AddPoint(polycoord[0][1],polycoord[0][0])

    # Create polygon
    searchArea = ogr.Geometry(ogr.wkbPolygon)
    searchArea.AddGeometry(ring)

    for x in range(len(self._buildPolyCollection)):
      if self._buildPolyCollection[x].Intersects(searchArea):
        return WALL
    return FREE

  #-----------------get Field Status-----------------------
  def getfieldStatus(self, x, y):
        if x >= len(self.field):
            return WALL
        if y >= len(self.field[x]):
            return WALL
        if x < 0 or y < 0:
            return WALL
        if self.field[x][y] is None:
            lat_UpLeft, lon_UpLeft = convertXY_toCords(x * self.tileSizeX, y * self.tileSizeY,
                                                       self.maxLatCoord, self.minLatCoord,
                                                       self.maxLonCoord, self.minLonCoord, WIDTH,
                                                       self.height)
            lat_UpRight, lon_UpRight = convertXY_toCords(self.tileSizeX * (x + 1), y * self.tileSizeY,
                                                         self.maxLatCoord, self.minLatCoord,
                                                         self.maxLonCoord, self.minLonCoord, WIDTH,
                                                         self.height)
            lat_BotLeft, lon_BotLeft = convertXY_toCords(x * self.tileSizeX, (y + 1) * self.tileSizeY,
                                                         self.maxLatCoord, self.minLatCoord,
                                                         self.maxLonCoord, self.minLonCoord, WIDTH,
                                                         self.height)
            lat_BotRight, lon_BotRight = convertXY_toCords((x + 1) * self.tileSizeX,
                                                           (y + 1) * self.tileSizeY, self.maxLatCoord,
                                                           self.minLatCoord, self.maxLonCoord,
                                                           self.minLonCoord, WIDTH, self.height)

            if ON_THE_FLYDB == 1:
                """ REWORK THIS
                with driver.session() as session:
                    self.field[x][y] = session.read_transaction(onTheFlyObstacleCheck, lat_UpLeft, lon_UpLeft,
                                                                      lat_UpRight, lon_UpRight, lat_BotLeft,
                                                                      lon_BotLeft, lat_BotRight, lon_BotRight)
                """
            else:
                polycoord = [(lat_UpLeft, lon_UpLeft), (lat_UpRight, lon_UpRight), (lat_BotRight, lon_BotRight),
                             (lat_BotLeft, lon_BotLeft)]
                self.field[x][y] = self.polygonOverlap(polycoord)
        return self.field[x][y]

  def setFieldStatus(self,x,y,value):
    self.field[x][y]=value

  def toConsole(self, msg):
    print("[MAP_Square]"+str(msg))

  def fieldToCordsOutput(self,x,y):
    lat, lon = convertXY_toCords(x * self.tileSizeX, y * self.tileSizeY,
                                                       self.maxLatCoord, self.minLatCoord,
                                                       self.maxLonCoord, self.minLonCoord, WIDTH,
                                                       self.height)
    self.toConsole("Lat: "+str(lat)+" | Lon: "+str(lon))
