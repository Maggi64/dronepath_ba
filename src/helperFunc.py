import heapq
import itertools

from math import sin, cos, sqrt, atan2, radians

class FastPriorityQueue():
    """
    A faster queue than queue.PriorityQueue
    Implementation copied from: https://docs.python.org/3.3/library/heapq.html
    """

    def __init__(self):
        self.pq = []  # list of entries arranged in a heap
        self.counter = itertools.count()  # unique sequence count

    def add_task(self, task, priority=0):
        'Add a new task'
        count = next(self.counter)
        entry = [priority, count, task]
        heapq.heappush(self.pq, entry)

    def pop_task(self):
        'Remove and return the lowest priority task. Raise KeyError if empty.'
        while self.pq:
            priority, count, task = heapq.heappop(self.pq)
            return task
        raise KeyError('pop from an empty priority queue')

    def empty(self):
        return len(self.pq) == 0

def ranges(N, nb):
    step = N / nb
    calculatedRange=[]
    for i in range(nb):
      curStart=round(step*(i))  
      curEnd=round(step*(i+1))
      stepRange=[curStart,curEnd]
      calculatedRange.append(stepRange)
    
    return calculatedRange
  
def calcDistance(lat_origin,lon_origin,lat_dest, lon_dest):
  # approximate radius of earth in km
  R = 6373.0
  
  lat1 = radians(lat_origin)
  lon1 = radians(lon_origin)
  lat2 = radians(lat_dest)
  lon2 = radians(lon_dest)
  
  dlon = lon2 - lon1
  dlat = lat2 - lat1
  
  a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
  c = 2 * atan2(sqrt(a), sqrt(1 - a))
  
  distance = R * c
  return distance

def convertXY_toCords(x,y,maxLatCoord,minLatCoord,maxLonCoord,minLonCoord,width_screen,height_screen):
    #print("X:"+str(x)+" Y:"+str(y))
    distanceDiffLat= maxLatCoord-minLatCoord
    distanceDiffLon= maxLonCoord-minLonCoord
    lon=(x/width_screen)*distanceDiffLon+minLonCoord
    lat=maxLatCoord-(y/height_screen)*distanceDiffLat
    return lat,lon