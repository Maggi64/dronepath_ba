from Pathfinding.AlgoAbstract import PathfindingAlgorithm
from constants import UNINITIALIZED,WALL
from helperFunc import FastPriorityQueue
import threading
import time
from timeit import default_timer as timer

class Pathfinder(PathfindingAlgorithm):

  def __init__(self,Map):
    self.mapDB=Map
    self.exploredFields=0
    algoThread = threading.Thread(target=self.run, args=())
    algoThread.start()
  
  def run(self):
    #HardCoded Start & Goal
    start=(442    , 393)
    goal=(179  , 154)
    while self.mapDB.mapReady==False:
      time.sleep(0.1)
    timestart = timer()
    self.toConsole(str(self.getPath(start,goal)))
    timeend = timer()
    self.toConsole("Dauer: " + str(timeend - timestart))
    self.toConsole("Explored Fields: "+str(self.exploredFields))

  def getPath(self, start, goal):
      frontier = FastPriorityQueue()
      frontier.add_task(start, 0)
      came_from = {}
      came_from[start] = None
      self.mapDB.setFieldStatus(start[0],start[1], 0)
      
      while not frontier.empty():
          current = frontier.pop_task()
          
          if current == goal:
              break
          
          for nextNode in self.neighbors(current):
              (nextX,nextY)= nextNode
              nextNodeStatus=self.mapDB.getfieldStatus(nextX,nextY)
              self.exploredFields=self.exploredFields+1
              if nextNodeStatus!= WALL:
                new_cost = nextNodeStatus + 1
                if nextNodeStatus==UNINITIALIZED or new_cost < nextNodeStatus:
                    self.mapDB.setFieldStatus(nextX,nextY,new_cost)
                    priority = new_cost + self.heuristic(goal, nextNode)
                    frontier.add_task(nextNode, priority)
                    came_from[nextNode] = current
      
      return came_from
    
  def neighbors(self, pos):
    (x, y) = pos
    results = [(x+1, y), (x, y-1), (x-1, y), (x, y+1)]
    return results
  
  def heuristic(self,a, b):
      (x1, y1) = a
      (x2, y2) = b
      return abs(x1 - x2) + abs(y1 - y2)
  
  def toConsole(self, msg):
    print("[A*_Algo]"+msg)