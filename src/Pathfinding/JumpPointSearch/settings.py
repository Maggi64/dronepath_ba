from Maps.MapSquare.settings import *
from pygame import font
font.init()
#-----------------------

SIDE = 15  # Start and goal squares side.
START_COLOR = (127, 177, 160)  # Start color = green.
GOAL_COLOR = (255,0,0)  # Goal color = red.
START_INIT_POS = (100,100)  # Start initial position.
GOAL_INIT_POS = (30,30)  # Goal initial position.



CAPTION = 'Drone Path Finding'  # Window's caption.

BG_COLOR = (255,255,255)  # Background color.
WIDTH_PATH = 8

GROUNDTILE_COLOR= (255,255,255)
OBSTACLE_COLOR= (121, 12, 169)
FREE_COLOR= (0,255,0)
PATH_COLOR = (255,0,0) # Path's edge color.
RECT_BORDER_WIDTH = 0

TEXT_X = WIDTH - 200  # 'x' coordinate where to display text info.
TEXT_Y = 15           # 'y' initial coordinate of text info.
TEXT_PADDING = 18     # Text padding between lines.
TEXT_COLOR = (255,255,0)  # Text color.
FONT = font.SysFont('Tahoma',15)  # Text font.
SMALL_FONT = font.SysFont('Tahoma',9)

