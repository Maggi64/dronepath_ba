import threading
import time
from timeit import default_timer as timer

from constants import *
from Pathfinding.AlgoAbstract import *
from .settings import *
from helperFunc import FastPriorityQueue
# Install these
import pygame as pg

#-----Helper Classes------
class Block(pg.sprite.Sprite):
  def __init__(self,color,pos):
    pg.sprite.Sprite.__init__(self)
    
    self.image = pg.Surface((SIDE,SIDE))
    self.rect = self.image.get_rect(topleft=pos)

    self.image.fill(color)

class FoundPath(Exception):
    """ Raise this when you found a path. it's not really an error,
    but I need to stop the program and pass it up to the real function"""
    pass
#---------------------------

class Pathfinder(PathfindingAlgorithm):
    def __init__(self,Map):
        pg.init()
        # MAP OBJECT
        self.mapDB = Map
        
        # --------------------------------------------------------------------------#
        #                      Initialise Display                                  #
        # --------------------------------------------------------------------------#
        self.screen = pg.display.set_mode((WIDTH, self.mapDB.height))

        # Display's icon and caption:
        pg.display.set_caption(CAPTION)

        # Start, goalPos and obstacles surface.
        self.startPos = Block(START_COLOR, START_INIT_POS)
        self.goalPos = Block(GOAL_COLOR, GOAL_INIT_POS)

        self.backgoundGrid = pg.surface.Surface((WIDTH, self.mapDB.height))
        self.backgoundText = pg.surface.Surface((WIDTH, self.mapDB.height))
        self.backgoundText.set_colorkey((0, 0, 0))

        self.positionBlocks = pg.sprite.Group(self.startPos, self.goalPos)
        self.drawGroundTiles(self.mapDB.squares_vert)
        self.drawAllFields()
        # States: 'normal', 'start_drag', 'goal_drag' 'running', 'path_found'.
        if self.mapDB.mapReady==False:
          self.state= 'building'
        else:
          self.state= 'normal'
      
        self.toConsole(self.state)
        
        self.exploredFields=0
        
        self._mainloop()

    # - - - - - - - - - - - - - - - - - - - - -

    def _mainloop(self):
        exitProgram = False
        firstRun = True
        self.live_render = 0
        self.exitPathfinding = False
        while not exitProgram:
            # ---------------------------
            # - Events handling:
            for e in pg.event.get():
                # If the user clicks the window's 'x', closes the app.
                if e.type == pg.QUIT:
                    exitProgram = True
                if self.state != 'running':
                    if e.type == pg.MOUSEBUTTONDOWN:
                        if e.button == 1:  # Left mouse button clicked:
                            if self.startPos.rect.collidepoint(e.pos):
                                self.state = 'start_drag'
                            elif self.goalPos.rect.collidepoint(e.pos):
                                self.state = 'goal_drag'
                        self.toConsole(self.state)

                    elif e.type == pg.MOUSEBUTTONUP:
                        self.state = 'normal';
                        self.toConsole(self.state)

                    elif e.type == pg.MOUSEMOTION:
                        if e.buttons[0]:  # Left mouse button holding:
                            if self.state == 'start_drag':
                                self.startPos.rect.center = e.pos
                            elif self.state == 'goal_drag':
                                self.goalPos.rect.center = e.pos

                    elif e.type == pg.KEYDOWN:
                        if e.key == pg.K_c:  # Clear obstacles on screen:
                            self.drawGroundTiles(self.mapDB.squares_vert)
                            self.toConsole('Obstacles surface cleared.')
                        elif e.key == pg.K_s:  # Save current obstacles surface:
                            self.drawAllFields()
                        elif e.key == pg.K_d:  # Debug show map
                            self.state = 'running';
                            self.toConsole(self.state)
                            loadMapThread = threading.Thread(target=self.mapDB.loadCompleteMap(), args=())
                            loadMapThread.start()
                        elif e.key == pg.K_RETURN:  # Enter pressed: run RRT algorithm.F
                            if firstRun == False:
                                self.toConsole("--Reload MAP--")
                                loadMapThread = threading.Thread(target=self.mapDB.loadCompleteMap(), args=())
                                loadMapThread.start()
                            self.state = 'running';
                            self.toConsole(self.state)
                            algoThread = threading.Thread(target=self.run, args=())
                            algoThread.start()
                            firstRun = False
                        elif e.key == pg.K_l:
                            if self.live_render == 0:
                                self.live_render = 1
                                self.toConsole("--Live Render ON--")
                            else:
                                self.live_render = 0
                                self.toConsole("--Live Render OFF--")
            # ---------------------------
            # Adjust refreshrate
            if self.state == 'running' and self.live_render == 0:
                time.sleep(1)
                if not algoThread.isAlive():
                    self.state = 'normal'
                    self.toConsole(self.state)
            elif self.state == 'running' and self.live_render == 1:
                time.sleep(0.1)
                self.updateScreen()
                if not algoThread.isAlive():
                    self.state = 'normal'
                    self.toConsole(self.state)
            elif self.state == 'building':
                time.sleep(1)
                self.updateScreen()
                if self.mapDB.mapReady==True:
                  self.state= 'normal'
                  self.toConsole(self.state)
            else:
                time.sleep(0.05)
                self.updateScreen()

    # - - - - - - - - - - - - - - - - - - - - - -

    # ----------------------------------------------------------------------------#
    #        Screen/Drawfunctions                                       )        #
    # ----------------------------------------------------------------------------#
    def updateScreen(self):
        self.screen.fill(BG_COLOR)
        self.screen.blit(self.backgoundGrid, (0, 0))
        self.screen.blit(self.backgoundText, (0, 0))
        self.positionBlocks.draw(self.screen)
        pg.display.flip()

    def drawPathline(self, sourceX, sourceY, endX, endY):
        sourceX = sourceX * self.mapDB.tileSizeX + 0.5 * self.mapDB.tileSizeX
        sourceY = sourceY * self.mapDB.tileSizeY + 0.5 * self.mapDB.tileSizeY
        endX = endX * self.mapDB.tileSizeX + 0.5 * self.mapDB.tileSizeX
        endY = endY * self.mapDB.tileSizeY + 0.5 * self.mapDB.tileSizeY
        pg.draw.line(self.backgoundGrid, PATH_COLOR, [sourceX, sourceY], [endX, endY], WIDTH_PATH)

    # draw entire map
    def drawAllFields(self):
        for x in range(len(self.mapDB.field)):
            for y in range(len(self.mapDB.field[x])):
                if self.mapDB.field[x][y] == OBSTACLE:
                    pg.draw.rect(self.backgoundGrid, OBSTACLE_COLOR, (
                    x * self.mapDB.tileSizeX, y * self.mapDB.tileSizeY, self.mapDB.tileSizeX, self.mapDB.tileSizeY), RECT_BORDER_WIDTH)
                elif self.mapDB.field[x][y] == FREE:
                    pg.draw.rect(self.backgoundGrid, FREE_COLOR, (
                    x * self.mapDB.tileSizeX-1, y * self.mapDB.tileSizeY, self.mapDB.tileSizeX, self.mapDB.tileSizeY), RECT_BORDER_WIDTH)

        self.updateScreen()

    def drawGroundTiles(self, squares_vert):
        # drawGroundTiles
        self.backgoundGrid.fill(BG_COLOR)
        for xTile in range(0, SQUARES_HORI):
            for yTile in range(0, squares_vert):
                tile_topLeftCordX = xTile * self.mapDB.tileSizeX
                tile_topLeftCordY = yTile * self.mapDB.tileSizeY
                pg.draw.rect(self.backgoundGrid, GROUNDTILE_COLOR,
                             (tile_topLeftCordX, tile_topLeftCordY, self.mapDB.tileSizeX, self.mapDB.tileSizeY), RECT_BORDER_WIDTH)

                # draw tileID
                if SQUARES_HORI < 31:
                    tileText = SMALL_FONT.render("[" + str(xTile) + "][" + str(yTile) + "]", 0, TEXT_COLOR, BG_COLOR)
                    textPosX = tile_topLeftCordX + self.mapDB.tileSizeX / 2 - 16
                    textPosY = tile_topLeftCordY + self.mapDB.tileSizeY / 2 - 9
                    self.backgoundText.blit(tileText, (textPosX, textPosY))

        self.updateScreen()

    # Color fields red,green,blue
    def drawFieldColor(self, x, y, path):
        if path == 1:
            pg.draw.rect(self.backgoundGrid, PATH_COLOR, (
            x * self.mapDB.tileSizeX, y * self.mapDB.tileSizeY, self.mapDB.tileSizeX, self.mapDB.tileSizeY), RECT_BORDER_WIDTH)
        else:
            if self.mapDB.field[x][y] == OBSTACLE:
                pg.draw.rect(self.backgoundGrid, OBSTACLE_COLOR, (
                x * self.mapDB.tileSizeX, y * self.mapDB.tileSizeY, self.mapDB.tileSizeX, self.mapDB.tileSizeY), RECT_BORDER_WIDTH)
            elif self.mapDB.field[x][y] == FREE:
                pg.draw.rect(self.backgoundGrid, FREE_COLOR, (
                x * self.mapDB.tileSizeX, y * self.mapDB.tileSizeY, self.mapDB.tileSizeX, self.mapDB.tileSizeY), RECT_BORDER_WIDTH)


    def fieldStatus(self,x,y):
      fieldStatus = self.mapDB.getfieldStatus(x,y)
      self.exploredFields=self.exploredFields+1
      if self.live_render==1:
        if not x >= len(self.mapDB.field):
          if not  y >= len(self.mapDB.field[x]): 
            if not (x < 0 or y < 0):
              self.drawFieldColor(x, y, 0)
      return fieldStatus
    # ----------------------------------------------------------------------------#
    #                 Start Pathfinding                                          #
    # ----------------------------------------------------------------------------#
    def run(self):
        # in which tile is the startPos and goalPos?
        startFieldX = int(self.startPos.rect.centerx / self.mapDB.tileSizeX)
        startFieldY = int(self.startPos.rect.centery / self.mapDB.tileSizeY)
        endFieldX = int(self.goalPos.rect.centerx / self.mapDB.tileSizeX)
        endFieldY = int(self.goalPos.rect.centery / self.mapDB.tileSizeY)
        
        #Set Pos
        """
        startFieldX = 665                       
        startFieldY = 324 
        endFieldX = 208                        
        endFieldY = 64
        """
        self.exploredFields=0
        self.toConsole("Start: X "+ str(startFieldX)+" | Y "+str(startFieldY))
        self.mapDB.fieldToCordsOutput(startFieldX,startFieldY)
        self.toConsole("Goal: X "+ str(endFieldX)+" | Y "+str(endFieldY))
        self.mapDB.fieldToCordsOutput(endFieldX,endFieldY)
        start = timer()
        path = self.getPath((startFieldX, startFieldY), (endFieldX, endFieldY))
        #Stop Timer
        self.end = timer()
        self.toConsole("----------------------------")
        self.toConsole("Dauer: " + str(self.end - start))
        self.toConsole("Path: " + str(path))
        self.toConsole("Explored Fields:" +str(self.exploredFields))
        self.toConsole("----------------------------")
        if self.live_render != 1:
            self.drawAllFields()
        
    def _exploreDiagonal(self, startX, startY, directionX, directionY):
        """
        Explores field along the diagonal direction for JPS, starting at point (startX, startY)

        Parameters
        startX, startY - the coordinates to startPos exploring from.
        directionX, directionY - an element from: {(1, 1), (-1, 1), (-1, -1), (1, -1)} corresponding to the x and y directions respectively.

        Return
        A 2-tuple containing the coordinates of the jump point if it found one
        None if no jumppoint was found.
        """
        cur_x, cur_y = startX, startY  # indices of current cell.
        curCost = self.mapDB.field[startX][startY]

        while (True):
            cur_x += directionX
            cur_y += directionY
            curCost += 1

            if self.fieldStatus(cur_x, cur_y) == UNINITIALIZED:
                self.mapDB.field[cur_x][cur_y] = curCost
                self.sources[cur_x][cur_y] = startX, startY
            elif cur_x == self.end_x and cur_y == self.end_y:  # destination found
                self.mapDB.field[cur_x][cur_y] = curCost
                self.sources[cur_x][cur_y] = startX, startY
                raise FoundPath()
            else:  # collided with an obstacle. We are done.
                return None

            # If a jump point is found,
            if self.fieldStatus(cur_x + directionX, cur_y) == OBSTACLE and self.fieldStatus(cur_x + directionX,
                                                                                            cur_y + directionY) != OBSTACLE:
                return (cur_x, cur_y)
            else:  # otherwise, extend a horizontal "tendril" to probe the field.
                self._queueJumpPoint(self._exploreCardinal(cur_x, cur_y, directionX, 0))

            if self.fieldStatus(cur_x, cur_y + directionY) == OBSTACLE and self.fieldStatus(cur_x + directionX,
                                                                                            cur_y + directionY) != OBSTACLE:
                return (cur_x, cur_y)
            else:  # extend a vertical search to look for anything
                self._queueJumpPoint(self._exploreCardinal(cur_x, cur_y, 0, directionY))

    def _exploreCardinal(self, startX, startY, directionX, directionY):
        """
        Explores field along a cardinal direction for JPS (north/east/south/west), starting at point (startX, startY)

        Parameters
        startX, startY - the coordinates to startPos exploring from.
        directionX, directionY - an element from: {(1, 1), (-1, 1), (-1, -1), (1, -1)} corresponding to the x and y directions respectively.

        Result:
        A 2-tuple containing the coordinates of the jump point if it found one
        None if no jumppoint was found.
        """
        cur_x, cur_y = startX, startY  # indices of current cell.
        curCost = self.mapDB.field[startX][startY]

        while (True):
            cur_x += directionX
            cur_y += directionY
            curCost += 1

            if self.fieldStatus(cur_x, cur_y) == UNINITIALIZED:
                self.mapDB.field[cur_x][cur_y] = curCost
                self.sources[cur_x][cur_y] = startX, startY
            elif cur_x == self.end_x and cur_y == self.end_y:  # destination found
                self.mapDB.field[cur_x][cur_y] = curCost
                self.sources[cur_x][cur_y] = startX, startY
                raise FoundPath()
            else:  # collided with an obstacle or previously explored part. We are done.
                return None

            # check neighbouring cells, i.e. check if cur_x, cur_y is a jump point.
            if directionX == 0:
                if self.fieldStatus(cur_x + 1, cur_y) == OBSTACLE and self.fieldStatus(cur_x + 1,
                                                                                       cur_y + directionY) != OBSTACLE:
                    return cur_x, cur_y
                if self.fieldStatus(cur_x - 1, cur_y) == OBSTACLE and self.fieldStatus(cur_x - 1,
                                                                                       cur_y + directionY) != OBSTACLE:
                    return cur_x, cur_y
            elif directionY == 0:
                if self.fieldStatus(cur_x, cur_y + 1) == OBSTACLE and self.fieldStatus(cur_x + directionX,
                                                                                       cur_y + 1) != OBSTACLE:
                    return cur_x, cur_y
                if self.fieldStatus(cur_x, cur_y - 1) == OBSTACLE and self.fieldStatus(cur_x + directionX,
                                                                                       cur_y - 1) != OBSTACLE:
                    return cur_x, cur_y

    def _queueJumpPoint(self, xy):
        """
        Add a jump point to the priority queue to be searched later. The priority is the minimum possible number of steps to the destination.
        Also check whether the search is finished.

        Parameters
        self.queue - a priority queue for the jump point search
        xy - 2-tuple with the coordinates of a point to add.

        Return
        None
        """
        if xy is not None:
            self.queue.add_task(xy,
                                self.mapDB.field[xy[0]][xy[1]] + max(abs(xy[0] - self.end_x), abs(xy[1] - self.end_y)))

    def _getPath(self, sources, start_x, start_y, end_x, end_y):
        """
        Reconstruct the path from the source information as given by jps(...).

        Parameters
        sources          - a 2d array of the predecessor to each node
        start_x, start_y - the x, y coordinates of the starting position
        end_x, end_y     - the x, y coordinates of the destination

        Return
        a list of jump points as 2-tuples (coordinates) starting from the startPos node and finishing at the end node.
        """
        result = []
        cur_x, cur_y = end_x, end_y

        
        while cur_x != start_x or cur_y != start_y:
            result.append((cur_x, cur_y))
            self.drawFieldColor(cur_x, cur_y, 1)
            self.drawPathline(sources[cur_x][cur_y][0], sources[cur_x][cur_y][1], cur_x, cur_y)
            cur_x, cur_y = sources[cur_x][cur_y]
        result.reverse()
        return [(start_x, start_y)] + result

    def getPath(self, start, end):
        self.start_x = int(start[0])
        self.start_y = int(start[1])
        self.end_x = int(end[0])
        self.end_y = int(end[1])

        # handle obvious exception cases: either startPos or end is unreachable
        if self.fieldStatus(self.start_x, self.start_y) == OBSTACLE:
            self.toConsole("Obstacle on startfield")
            return None
        if self.fieldStatus(self.end_x, self.end_y) == OBSTACLE:
            self.toConsole("Obstacle on endfield")
            return None

        # MAIN JPS FUNCTION
        # Initialize some arrays and certain elements.
        self.sources = [[(None, None) for i in self.mapDB.field[0]] for j in
                        self.mapDB.field]  # the jump-point predecessor to each point.
        self.mapDB.field[self.start_x][self.start_y] = 0
        self.mapDB.field[self.end_x][self.end_y] = DESTINATION

        self.queue = FastPriorityQueue()
        self._queueJumpPoint((self.start_x, self.start_y))

        # Main loop: iterate through the queue
        while (not self.queue.empty()):
            pX, pY = self.queue.pop_task()

            try:
                self._queueJumpPoint(self._exploreCardinal(pX, pY, 1, 0))
                self._queueJumpPoint(self._exploreCardinal(pX, pY, -1, 0))
                self._queueJumpPoint(self._exploreCardinal(pX, pY, 0, 1))
                self._queueJumpPoint(self._exploreCardinal(pX, pY, 0, -1))

                self._queueJumpPoint(self._exploreDiagonal(pX, pY, 1, 1))
                self._queueJumpPoint(self._exploreDiagonal(pX, pY, 1, -1))
                self._queueJumpPoint(self._exploreDiagonal(pX, pY, -1, 1))
                self._queueJumpPoint(self._exploreDiagonal(pX, pY, -1, -1))
            except FoundPath:
                return self._getPath(self.sources, self.start_x, self.start_y, self.end_x, self.end_y)
        return None
    
    def toConsole(self, msg):
      print("[JPS_Algo]"+msg)
# - - - - - - - - - - - - -- - - - - - - -
