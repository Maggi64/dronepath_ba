import abc
class PathfindingAlgorithm(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def run(self): 
      pass
    
    @abc.abstractmethod
    def getPath(self):
      return
    
    @abc.abstractmethod
    def __init__(self,Map):
      pass
    
    @abc.abstractmethod #Print to console always with modulename in front (i.e MAP: [MESSAGE])
    def toConsole(self,msg):
      pass