from Pathfinding.JumpPointSearch.jps_main import Pathfinder as jps 
from Pathfinding.AStar.AStar_main import Pathfinder as astar
from Maps.MapSquare.mapSquareGrid import MapSquareGrid as mapSG
from Databases.Neo4J.neo4jDB import neo4jDB as neoDB
  
if __name__ == '__main__':
  jps(mapSG(neoDB()))
  #astar(mapSG(neoDB()))